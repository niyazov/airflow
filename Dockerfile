FROM python:3.8

RUN apt-get update && apt-get install shellcheck

RUN pip3 install apache-airflow==1.10.12 --constraint "https://raw.githubusercontent.com/apache/airflow/constraints-1.10.12/constraints-3.8.txt"
RUN pip3 install pylint pylint-exit kubernetes awscli SQLAlchemy

RUN airflow initdb

RUN addgroup --gid 1001 jenkins && \
    adduser --disabled-password --uid 1001 --ingroup jenkins jenkins

# RUN addgroup --gid $JENKINS_USER_ID $JENKINS_USER_NAME && \
#     adduser --disabled-password --uid $JENKINS_USER_ID --ingroup $JENKINS_USER_NAME $JENKINS_USER_NAME

# WORKDIR $JENKINS_WORKDIR

# USER $JENKINS_USER_NAME

WORKDIR /home/jenkins

USER 1001